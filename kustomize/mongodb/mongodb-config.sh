kubectl delete secret mongodb-secret -n workshop-nodejs
kubectl create secret generic mongodb-secret -n workshop-nodejs \
  --from-literal=MONGO_INITDB_ROOT_USERNAME=test \
  --from-literal=MONGO_INITDB_ROOT_PASSWORD=test34679

kubectl -n workshop-nodejs exec -it pod/mongodb-0 -c mongod /bin/bash
db.getSiblingDB("admin").createUser({user: "test", pwd: "test34679", roles:[{role:"root",db:"admin"}]})
