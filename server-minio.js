const express = require('express');
const Minio = require('minio');
const multer = require('multer');
const fs = require('fs'); 

const app = express();
const port = 3000;

// Configure MinIO client
const minioClient = new Minio.Client({
  endPoint: 'minio.nursenu.pro',
  port: 443, // 9000
  useSSL: true, // false
  accessKey: 'SC8YO9DAS8FN745V3D6G',
  secretKey: 'jQsNNBqTpgouf2lwhzg9cXvBIunUnQtfmEYB5BOf'
});

// Middleware to parse JSON bodies
app.use(express.json());

// Basic route
// app.get('/', (req, res) => {
//   res.send('Hello, MinIO!');
// });

// Serve static files (for the HTML file)
app.use(express.static('public'));

// List all buckets
app.get('/buckets', async (req, res) => {
  try {
    const buckets = await minioClient.listBuckets();
    res.status(200).json(buckets);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error listing buckets' });
  }
});
//curl http://localhost:3000/buckets

// Create a new bucket
app.post('/buckets', async (req, res) => {
  const { bucketName } = req.body;
  try {
    await minioClient.makeBucket(bucketName, 'us-east-1');
    res.status(201).json({ message: `Bucket '${bucketName}' created successfully` });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error creating bucket' });
  }
});
//curl -X POST http://localhost:3000/buckets -H "Content-Type: application/json" -d '{"bucketName": "citcoms"}'

// Middleware to handle file uploads
const upload = multer({ dest: 'uploads/' });

// Upload a file to a bucket
app.post('/upload/:bucketName', upload.single('file'), (req, res) => {

  const file = req.file;
  // console.log(file)
  if (!file) {
    return res.status(400).send('No file uploaded.');
  }

  const { originalname, path } = file;
  const objectName = originalname;

  const { bucketName } = req.params;
  // console.log(bucketName)
  minioClient.fPutObject(bucketName, objectName, path, (err, etag) => {
    if (err) {
      console.error(err);
      return res.status(500).json({ message: 'Error uploading file' });
    }
    fs.unlinkSync(path);
    res.status(201).json({ message: 'File uploaded successfully', etag });
  });
});
//curl -X POST http://localhost:3000/upload/citcoms --form 'file=@"/C:/Users/Theerawut/Desktop/workshop-nodejs/my-file.txt"'

// List objects in a bucket
app.get('/buckets/:bucketName/objects', async (req, res) => {
  const { bucketName } = req.params;
  try {
    const stream = minioClient.listObjectsV2(bucketName, '', true);
    const objects = [];
    stream.on('data', obj => objects.push(obj));
    stream.on('end', () => res.status(200).json(objects));
    stream.on('error', err => {
      console.error(err);
      res.status(500).json({ message: 'Error listing objects' });
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error listing objects' });
  }
});
//curl http://localhost:3000/buckets/citcoms/objects

// Download a file from a bucket
app.get('/buckets/:bucketName/objects/:objectName', (req, res) => {
  const { bucketName, objectName } = req.params;
  minioClient.getObject(bucketName, objectName, (err, dataStream) => {
    if (err) {
      console.error(err);
      return res.status(500).json({ message: 'Error downloading file' });
    }
    dataStream.pipe(res);
  });
});
//curl http://localhost:3000/buckets/citcoms/objects/my-file.txt --output my-file.txt

// Start the server
app.listen(port, () => {
  console.log(`Server running at port:${port}`);
});
