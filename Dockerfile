# FROM node:18 AS deps
# WORKDIR /usr/src/app
# COPY package.json ./
# RUN npm install && npm cache clean --force
# #-------------------------------------------

# FROM node:18 AS builder
# WORKDIR /usr/src/app
# COPY --from=deps /usr/src/app/node_modules ./node_modules
# COPY . .
# #-------------------------------------------
  
# FROM node:18 AS runner
# WORKDIR /usr/src/app
# COPY --from=builder /usr/src/app/node_modules ./node_modules
# COPY --from=builder /usr/src/app/lib/*.js ./lib/
# COPY --from=builder /usr/src/app/public/*.js ./public/
# COPY --from=builder /usr/src/app/server.js ./server.js
# COPY --from=builder /usr/src/app/package.json ./package.json

FROM node:18

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .
