const express = require('express')
const { MongoClient, ObjectId } = require('mongodb')
const app = express()
const port = 3000;

const url = 'mongodb://root:Nuadmin12345@45.77.36.236:27017'
const dbName = 'citcoms'
let db

// Middleware to parse JSON bodies
app.use(express.json())

// Connect to MongoDB
MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, (err, client) => {
  if (err) return console.error(err)
  db = client.db(dbName)
  console.log(`Connected to database: ${dbName}`)

  // Start the server after the database connection is established
  app.listen(port, () => {
    console.log(`Server running at port ${port}`)
  })
})

// Basic route
app.get('/', (req, res) => {
  res.send('Hello, world!')
})

//Create CRUD Endpoints

//Create a Document
app.post('/documents', async (req, res) => {
  try {
    const document = req.body;
    const result = await db.collection('documents').insertOne(document);
    res.status(201).json({ _id: result.insertedId, ...document });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error creating document' });
  }
});
//curl -X POST http://localhost:3000/documents -H "Content-Type: application/json" -d '{"name": "Document 1", "content": "This is the first document"}'

//Read Documents
app.get('/documents', async (req, res) => {
  try {
    const documents = await db.collection('documents').find().toArray();
    res.status(200).json(documents);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error fetching documents' });
  }
});
//curl http://localhost:3000/documents

app.get('/documents/:id', async (req, res) => {
  try {
    const document = await db.collection('documents').findOne({ _id: new ObjectId(req.params.id) }); 
    //ใส่เพิ่ม const { MongoClient, ObjectID } = require('mongodb');
    if (!document) {
      res.status(404).json({ message: 'Document not found' });
    } else {
      res.status(200).json(document);
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error fetching document' });
  }
});
//curl http://localhost:3000/documents/


//Update a Document
app.put('/documents/:id', async (req, res) => {
  try {
    const updates = req.body;
    const result = await db.collection('documents').updateOne(
      { _id: new ObjectId(req.params.id) },
      { $set: updates }
    );
    if (result.matchedCount === 0) {
      res.status(404).json({ message: 'Document not found' });
    } else {
      res.status(200).json({ message: 'Document updated' });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error updating document' });
  }
});
//curl -X PUT http://localhost:3000/documents/60c72b2f9b1d4c30d8f36b2e -H "Content-Type: application/json" -d '{"name": "Updated Document 1"}'


//Delete a Document
app.delete('/documents/:id', async (req, res) => {
  try {
    const result = await db.collection('documents').deleteOne({ _id: new ObjectId(req.params.id) });
    if (result.deletedCount === 0) {
      res.status(404).json({ message: 'Document not found' });
    } else {
      res.status(200).json({ message: 'Document deleted' });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Error deleting document' });
  }
});
//curl -X DELETE http://localhost:3000/documents/60c72b2f9b1d4c30d8f36b2e
