# API Specification Doc 1.0.1 <a name="index"></a>

**คู่มือการใช้งาน API สำหรับ Update ข้อมูลระหว่างระบบ DMC และ CCT**

-- --

#### API List

- URL: https://dmc-exchange.thaieduforall.org

| API Name         | API Desc                                |Detail |
| -----------------|:--------------------------------------- |------ |
| /auth/login      | ใช้สำหรับ login เพื่อรับ token ในการใช้งาน API |<a href="#login">Click</a>|
| /api/student| ใช้สำหรับ Update ข้อมูลนักเรียน                           |<a href="#student">Click</a>|

-- --


**1.	Service: Login ใช้สำหรับ Login เพื่อรับ token ในการใช้งาน API** <a name="login"></a>


```
URL: { base_path }/auth/login
Content-Type: application/json
Method: POST
```

| Type          | Parameters   | Values  |
| ------------- |:-------------|:------- |
| POST          | username     | ชื่อผู้ใช้   |
| POST          | password     | รหัสผ่าน  |


###### ตัวอย่างการส่ง (Request)
```javascript
{ 
  method: 'POST',
  url: ‘{base_path }/auth/login’,
  headers: { 'content-type': 'application/json' },
  body: { username: '[USERNAME]', password: '[PASSWORD]' },
  json: true 
};
```

###### ตัวอย่าง Response Status 200 OK

```javascript
{
  "user": 
	{
    "_id": "f66f8ec0503f11eba0e677eaf1017669",
    "user": "username",
    "application": "dmc-exchange",
    "profile": {
        "title": "นาย",
        "firstname": "ทดสอบ",
        "lastname": "นามสกุลทดสอบ",
        "email": "email@gmail.com",
        "tel": "0988888888"
    },
    "roles": [
        "exchange"
    ],
    "timestamp": "1600018448906"
  },
  "token": "eyJhbGcxxxxxxxxxxxxxxxxx.xxxxx"
}
```

###### ตัวอย่าง Response Status 400 Bad Request

```javascript
{
  "message": "Invalid Username or Password",
  "user": false
}
```
<a href="#index">Back to Top</a>


**2.	Service: student ใช้สำหรับ Update ข้อมูล** <a name="student"></a>
***Request***

```
Content-Type: application/json
Method: POST
URL: {base_path}/api/student
```

| Type          | Parameters   | Values  |
| ------------- |:-------------|:------- |
| HEADERS       | Authorization     | JWT (เว้นวรรค + token ที่ได้จากการ login ในข้อ 1)   |
| POST          | BODY     | ข้อมูล Field Name ที่จะอัพเดท (<a href="#datadic">ตารางคำอธิบายข้อมูล</a> หรือ Field Name ยึดตามที่ DMC จัดเก็บ) |


###### ตัวอย่างการส่ง (Request)

```javascript
{ 
  method: 'POST',
	url: ‘{base_path }/api/student ‘,
	headers: 
	{ 
    'Authorization': 'JWT eyJhbGciOiJSUzIxccxcxc9.eyJfaWQiOiJjMxxx',
	  'content-type': 'application/json' 
	},
	body: { 
    'cid': 'เลขประจำตัวประชาชน',
    'name': 'ชื่อ',
    'lastname': 'นามสกุล',
    'oid' : 'oid'
     ...
  },
  json: true
};
```

###### ตัวอย่าง Response Status 200 OK


```javascript
{
  "ok": true,
  "message": "successfully",
  "id": "f66f8ec0503f11eba0e677eaf1017669"
}
```

###### ตัวอย่าง Response Status 401 Unauthorized

```javascript
Unauthorized
```

###### ตัวอย่าง Response Status 400 body missing or invalid object format

```javascript
{
    "ok": false,
    "message": "body missing or invalid object format"
}
```

<a href="#index">Back to Top</a>


**ตารางคำอธิบายข้อมูล** <a name="datadic"></a>

|Field Name|Description|
|:-------------|:-------| 
|hostid | รหัสโรงเรียน |
|host | ชื่อโรงเรียน |
|cid | เลขประจำตัวประชาชน |
|class | ชั้น |
|room | ห้อง |
|pTel  |หมายเลขโทรศัพท์ของผู้ปกครอง|
|  |รหัสประจำบ้าน (ทะเบียนบ้าน)|
|new_houseno  |เลขที่บ้าน (ที่อยู่ปัจจุบัน)|
|new_moo  |หมู่ (ที่อยู่ปัจจุบัน)|
|new_road  |ถนน (ที่อยู่ปัจจุบัน)|
|new_tumbon  |ตำบล (ที่อยู่ปัจจุบัน)|
|new_city  |อำเภอ (ที่อยู่ปัจจุบัน)|
|new_province  |จังหวัด (ที่อยู่ปัจจุบัน)|
|new_zipcode  |รหัสไปรษณีย์ (ที่อยู่ปัจจุบัน)|
|  |หมายเลขโทรศัพท์ (ที่อยู่ปัจจุบัน)|
|oid | OID |

<a href="#index">Back to Top</a>

## Contact


Author : Theerawut Thaweephattharawong

Email : [theerawutt53@gmail.com](Theerawutt53@gmail.com)


## License
[Inforvation Co.,Ltd](http://inforvation.com/)
